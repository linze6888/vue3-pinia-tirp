import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import Components from 'unplugin-vue-components/vite';
import { VantResolver } from 'unplugin-vue-components/resolvers';
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    Components({
      resolvers: [VantResolver()],
    }), 


  ],
  configureWebpack: {
    externals: {
      AMap: 'AMap'
    }
  },

  // server: {
  //   port: 3000,
  //   proxy: {
  //     '/api': {
  //       // target: 'http://101.200.86.21:1234',
  //       // target: 'http://152.136.185.210:5000',
  //       changeOrigin: true,
  //       rewrite: (p) => p.replace(/^\/api/, '')
  //     }
  //   }
  // },
  resolve:{
    alias:{
      '@':resolve('src')
    }
  }
})



