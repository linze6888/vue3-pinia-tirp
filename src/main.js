import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index";
import pinia from "./stores/index";
// import "normalize.css";
import "./assets/css/index.css";
// import zeRequest from './services/request/index'
const app = createApp(App);
app.use(router);
app.use(pinia);
app.mount("#app");

// zeRequest.get({
//     url:'/city/all',
// }).then(res =>{
//     console.log(res.data);
// })


// const instancel = axios.create({
//   timeout: 5000,
// });

// instancel.interceptors.request.use((config) => {
//   console.log("请求成功的拦截", config);
//   return config;
// }),
//   (error) => {
//     console.log("请求失败的拦截", error);
//     return config;
//   };
// instancel.interceptors.response.use(
//   (res) => {
//     console.log("响应应成功的拦截", res.data);
//     return res.data;
//   },
//   (error) => {
//     console.log("响应应成功的拦截", error);
//     return error;
//   }
// ),
//   // 1.发送request
//   instancel
//     .request({
//       url: "api/books",
//       method: "get",
//     })
//     .then((res) => {
//       console.log(res);
//     });

// // 1.发送request
// instancel.request({
//     url:'api/books',
//     method:'get',
//     timeout:10000,
// }).then(res =>{
//     console.log(res.data);
// })

//  2.get
// instancel.get('http://123.207.32.32:9001/lyric',{
//     params:{
//         id:500665345
//     }
// }).then(res =>{
//     console.log(res.data);
// })

// 3.post
// instancel
//   .post("api/books", {
//     type: "22",
//     name: '33',
//     description:'44'
//   })
//   .then((res) => {
//     console.log(res.data);
//   });
