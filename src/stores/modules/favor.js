import {getFavor} from '@/services'

import { defineStore } from "pinia"
const useFavorStore = defineStore("favor", {
	state: () => ({
		favor:[]
	}),
	actions: {
		async fetchFavorData() {
			const res = await getFavor()
			this.favor = res.data.data
		}
	}
})
export default useFavorStore
