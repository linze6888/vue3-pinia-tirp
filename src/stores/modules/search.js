import { defineStore } from 'pinia'
import { getSearchConditions, getSearchHouse } from '@/services'

const UseSearchStore = defineStore('Search', {
  state: () => ({
    searchConditions: [],
    searchHouse: []
  }),
  getters: {},
  actions: {
    async fetchSearchConditions() {
      const res = await getSearchConditions()
      this.searchConditions = res.data.data
    },
    async fetchResultHouse() {
      const res = await getSearchHouse()
      this.searchHouse = res.data.data
    }
  }
})
export default UseSearchStore
