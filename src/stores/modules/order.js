import {getOrderList} from '@/services'

import { defineStore } from "pinia"
const useOrderListStore = defineStore("order", {
	state: () => ({
		order:[]
	}),
	actions: {
		async fetchOrderListData() {
			const res = await getOrderList()
			this.order = res.data.data || []  
		}
	}
})
export default useOrderListStore
