import { getFavorHistory } from '@/services'

import { defineStore } from 'pinia'

const useFavorHistoryStore = defineStore('history', {
  state: () => ({
    history: []
  }),
  actions: {
    async fetchFavorHistoryData() {
      const res = await getFavorHistory()
      this.history = res.data.data
    }
  }
})

export default useFavorHistoryStore