import zeRequest from '../request'

export function getSearchConditions(){
    return zeRequest.get({
        url:'/search/top'
    })
}
export function getSearchHouse(){
    return zeRequest.get({
        url:'/search/result'
    })
}