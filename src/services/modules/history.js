import zeRequest from "../request"

export function getFavorHistory() {
	return zeRequest.get({
		url: "/favor/history"
	})
}
