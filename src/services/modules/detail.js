import zeRequest from "../request"

export function getDetailInfos(houseId) {
	return zeRequest.get({
		url: "/detail/infos",
		params: {
			houseId
		}
	})
}
