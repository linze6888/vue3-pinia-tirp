import zeRequest from '../request'

export function getOrderList(type = 'all') {
  return zeRequest.get({
    url: `/order/list?type=${type}`
  })
}
// export function getOrderList() {
//    return zeRequest.get({
//       url: '/order/list?type=all'
//      })
//    }