import zeRequest from "../request"

export function getCityAll() {
	return zeRequest.get({
		url: "/city/all"
	})
}
