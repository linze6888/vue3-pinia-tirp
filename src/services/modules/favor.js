import zeRequest from "../request"

export function getFavor() {
	return zeRequest.get({
		url: "/favor/list"
	})
}
